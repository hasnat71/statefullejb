package Ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;

/**
 *
 * @author Sayem
 */
@Stateful
public class ListElements implements ListElementsRemote {
    
    List<Integer> values = new ArrayList<>();

    @Override
    public void addElement(int e) {
      values.add(e);
    }

    @Override
    public void removeElement(int e) {
       values.remove(e);
    }

    @Override
    public List<Integer> getElement() {
        return values;
    }

}
