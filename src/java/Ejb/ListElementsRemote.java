
package Ejb;

import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Sayem
 */

@Remote
public interface ListElementsRemote {
    
    void addElement(int e);
    void removeElement(int e);
    
    List<Integer> getElement();
   
    
}
