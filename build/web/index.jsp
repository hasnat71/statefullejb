<%-- 
    Document   : index
    Created on : Aug 2, 2021, 9:52:34 AM
    Author     : Sayem Hasnat
--%>

<%@page import="java.util.List"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="Ejb.ListElementsRemote"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%!
    private static ListElementsRemote values;

    public void jspInit() {
        try {
            InitialContext ic = new InitialContext();
            values = (ListElementsRemote) ic.lookup("java:global/StatefulEjbPractise/ListElements");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

%>

<%
    if(request.getParameter("addNum")!= null){
    int e = Integer.parseInt(request.getParameter("t1"));
    values.addElement(e);
}
    if(request.getParameter("remNum") != null){
        int e = Integer.parseInt(request.getParameter("t1"));
        values.removeElement(e);
    }
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Welcome</h1>
        <form name = "ejbForm" method ="post">
            <input type='text' name="t1"><br>
            <input type="submit" value="Add" name="addNum"> <br>
            <input type="submit" value="Remove" name="remNum"> <br>

            
            <% 
if (values!=null){
    List<Integer> nums = values.getElement();
    for(int value : nums){
        out.println("<br>"+value);
    }
    out.println("<br>Size "+nums.size());
    
}
            %>
        </form>
    </body>
</html>
